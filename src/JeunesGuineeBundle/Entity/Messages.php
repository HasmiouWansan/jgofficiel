<?php

namespace JeunesGuineeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Messages
 *
 * @ORM\Table(name="messages")
 * @ORM\Entity(repositoryClass="JeunesGuineeBundle\Repository\MessagesRepository")
 */
class Messages
{
        public function __construct(){
        $this->date = new \DateTime('now');
        $this->natureMsg =" ";
        $this->view=false;
    }
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="emailExp", type="string", length=255)
     */
    private $emailExp;

    /**
     * @var string
     *
     * @ORM\Column(name="emailRec", type="string", length=255)
     */
    private $emailRec;

    /**
     * @var string
     *
     * @ORM\Column(name="sujet", type="string", length=255)
     */
    private $sujet;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="natureMsg", type="string", length=100)
     */
    private $natureMsg;

    /**
     * @var bool
     *
     * @ORM\Column(name="view", type="boolean")
     */
    private $view;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set emailExp
     *
     * @param string $emailExp
     * @return Messages
     */
    public function setEmailExp($emailExp)
    {
        $this->emailExp = $emailExp;

        return $this;
    }

    /**
     * Get emailExp
     *
     * @return string 
     */
    public function getEmailExp()
    {
        return $this->emailExp;
    }

    /**
     * Set emailRec
     *
     * @param string $emailRec
     * @return Messages
     */
    public function setEmailRec($emailRec)
    {
        $this->emailRec = $emailRec;

        return $this;
    }

    /**
     * Get emailRec
     *
     * @return string 
     */
    public function getEmailRec()
    {
        return $this->emailRec;
    }

    /**
     * Set sujet
     *
     * @param string $sujet
     * @return Messages
     */
    public function setSujet($sujet)
    {
        $this->sujet = $sujet;

        return $this;
    }

    /**
     * Get sujet
     *
     * @return string 
     */
    public function getSujet()
    {
        return $this->sujet;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Messages
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Messages
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set natureMsg
     *
     * @param string $natureMsg
     * @return Messages
     */
    public function setNatureMsg($natureMsg)
    {
        $this->natureMsg = $natureMsg;

        return $this;
    }

    /**
     * Get natureMsg
     *
     * @return string 
     */
    public function getNatureMsg()
    {
        return $this->natureMsg;
    }

    /**
     * Set view
     *
     * @param boolean $view
     * @return Messages
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return boolean 
     */
    public function getView()
    {
        return $this->view;
    }
}
