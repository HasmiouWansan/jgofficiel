<?php

namespace JeunesGuineeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Representants
 *
 * @ORM\Table(name="representants")
 * @ORM\Entity(repositoryClass="JeunesGuineeBundle\Repository\RepresentantsRepository")
 */
class Representants
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *@ORM\ManyToOne(targetEntity="JeunesGuineeBundle\Entity\Coordinations", inversedBy="representants", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false) 
     */
   private $coordinations;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255)
     */
    private $nom;

    /**
     *@ORM\OneToOne(targetEntity="JeunesGuineeBundle\Entity\Medias",cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true) 
     */
   private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="fonction", type="string", length=255)
     */
    private $fonction;

    /**
     * @var string
     *
     * @ORM\Column(name="biographie", type="text")
     */
    private $biographie;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Representants
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set fonction
     *
     * @param string $fonction
     * @return Representants
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction
     *
     * @return string 
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set biographie
     *
     * @param string $biographie
     * @return Representants
     */
    public function setBiographie($biographie)
    {
        $this->biographie = $biographie;

        return $this;
    }

    /**
     * Get biographie
     *
     * @return string 
     */
    public function getBiographie()
    {
        return $this->biographie;
    }

    /**
     * Set coordinations
     *
     * @param \JeunesGuineeBundle\Entity\Coordinations $coordinations
     * @return Representants
     */
    public function setCoordinations(\JeunesGuineeBundle\Entity\Coordinations $coordinations)
    {
        $this->coordinations = $coordinations;

        return $this;
    }

    /**
     * Get coordinations
     *
     * @return \JeunesGuineeBundle\Entity\Coordinations 
     */
    public function getCoordinations()
    {
        return $this->coordinations;
    }

    /**
     * Set photo
     *
     * @param \JeunesGuineeBundle\Entity\Medias $photo
     * @return Representants
     */
    public function setPhoto(\JeunesGuineeBundle\Entity\Medias $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return \JeunesGuineeBundle\Entity\Medias 
     */
    public function getPhoto()
    {
        return $this->photo;
    }
}
