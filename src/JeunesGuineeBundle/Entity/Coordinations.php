<?php

namespace JeunesGuineeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coordinations
 *
 * @ORM\Table(name="coordinations")
 * @ORM\Entity(repositoryClass="JeunesGuineeBundle\Repository\CoordinationsRepository")
 */
class Coordinations
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
   * @ORM\OneToMany(targetEntity="JeunesGuineeBundle\Entity\Representants", mappedBy="coordinations")
   */
    private $representants;

    /**
     * @var string
     *
     * @ORM\Column(name="appelation", type="string", length=255)
     */
    private $appelation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set appelation
     *
     * @param string $appelation
     * @return Coordinations
     */
    public function setAppelation($appelation)
    {
        $this->appelation = $appelation;

        return $this;
    }

    /**
     * Get appelation
     *
     * @return string 
     */
    public function getAppelation()
    {
        return $this->appelation;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Coordinations
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Coordinations
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Coordinations
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Coordinations
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->representants = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add representants
     *
     * @param \JeunesGuineeBundle\Entity\Representants $representants
     * @return Coordinations
     */
    public function addRepresentant(\JeunesGuineeBundle\Entity\Representants $representants)
    {
        $this->representants[] = $representants;

        return $this;
    }

    /**
     * Remove representants
     *
     * @param \JeunesGuineeBundle\Entity\Representants $representants
     */
    public function removeRepresentant(\JeunesGuineeBundle\Entity\Representants $representants)
    {
        $this->representants->removeElement($representants);
    }

    /**
     * Get representants
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRepresentants()
    {
        return $this->representants;
    }
}
