<?php

namespace JeunesGuineeBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * RepresentantsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RepresentantsRepository extends EntityRepository
{
	public function byCordination($id)
	{
	    $qb = $this->createQueryBuilder('r');
	    $qb->select('r');
	    $qb->where('r.coordinations = :val');
	    $qb->setParameter('val', $id);

	    return $qb->getQuery()->getResult();
	}
}
