<?php

namespace JeunesGuineeBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CoordinationsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CoordinationsRepository extends EntityRepository
{
}
