<?php

namespace JeunesGuineeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MembresType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('civilite','choice',array('choices'=>array('Mr'=>'Mr','Mme'=>'Mme')))
                ->add('nom',null,array('attr'=>array('required'=>true,'class'=>'input','placeholder'=>'Nom')))
                ->add('prenoms',null,array('attr'=>array('required'=>true,'class'=>'input','placeholder'=>'Prenoms')))
                ->add('birthday','date',[
                'format'=>'d-M-y',
                'years'=>range(1950, date("y"))
            ])
                ->add('email','email',array('required'=>false,'attr'=>array('class'=>'input','placeholder'=>'Email')))
                ->add('telephone',null,array('required'=>true,'attr'=>array('class'=>'input','placeholder'=>'Téléphone')))
                ->add('adresse','textarea',array('required'=>false,'attr'=>array('class'=>'input','placeholder'=>'Adresse')))
                ->add('motif','textarea',array('required'=>false,'attr'=>array('class'=>'ckeditor','placeholder'=>'Motivation')))
                ->add('activite',null,array('required'=>false,'attr'=>array('class'=>'input','placeholder'=>'Profession')))
                ->add('accordRI')
                ->add('valide');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JeunesGuineeBundle\Entity\Membres'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'jeunesguineebundle_membres';
    }


}
