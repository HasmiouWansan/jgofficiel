<?php

namespace JeunesGuineeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvenementsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titre','text',array('required'=>true, 
                                    'label'=>false,
                                    'attr'=>array('class'=>'input',
                                                  'placeholder'=>'Titre Ex: Reunion mensuelle')))
                ->add('description','textarea',array('required'=>true, 
                                    'label'=>false,
                                    'attr'=>array('class'=>'ckeditor',
                                                  'placeholder'=>'Description Ex: Un rassemblement générale du bureau éxécutif')))
                ->add('dateDebut','date',array('required'=>true
                                               
                                               ))
                ->add('heureDebut','text',array('required'=>false,
                                              'label'=>false,
                                              'attr'=>array('placeholder'=>'HH:MM')
                                              ))
                ->add('dateFin','date')
                ->add('heureFin','text',array('required'=>false,
                                              'label'=>false,
                                              'attr'=>array('placeholder'=>'HH:MM')
                                              ))
                ->add('lieu','text',array('required'=>FALSE, 
                                    'label'=>false,
                                    'attr'=>array('class'=>'input',
                                                  'placeholder'=>'Lieu Ex: Maison de la jeunesse, 34 Rue André, 75000 Paris')))
                ->add('publier');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JeunesGuineeBundle\Entity\Evenements'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'jeunesguineebundle_evenements';
    }


}
