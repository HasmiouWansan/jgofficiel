<?php

namespace JeunesGuineeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('emailExp','email',array('required'=>true,'attr'=>array('class'=>'input','placeholder'=>'Votre Email')))
                ->add('sujet',null,array('required'=>true,'attr'=>array('class'=>'input','placeholder'=>'Objet du message')))
                ->add('message','textarea',array('required'=>true,'attr'=>array('class'=>'input','placeholder'=>'Votre message')));
               
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JeunesGuineeBundle\Entity\Messages'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'jeunesguineebundle_messages';
    }


}


