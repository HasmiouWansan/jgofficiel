<?php

namespace JeunesGuineeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JeunesGuineeBundle\Entity\Medias;

class SlidesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titre',null,array('attr'=>array('class'=>'input',
                                                               'placeholder'=>'Titre du Diapo')))
               
                ->add('lien_photo1',null,array('attr'=>array('class'=>'input',
                                                               'placeholder'=>'Lien de la publication')))
                ->add('photo1', new MediasType())

                ->add('lien_photo2',null,array('attr'=>array('class'=>'input',
                                                               'placeholder'=>'Lien de la publication')))
                ->add('photo2', new MediasType())

                ->add('lien_photo3',null,array('attr'=>array('class'=>'input',
                                                               'placeholder'=>'Lien de la publication')))
                ->add('photo3', new MediasType())

        ;
    }
    
    /**
     * {@inheritdoc}
     */
   /* public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JeunesGuineeBundle\Entity\Slides'
        ));
    }*/

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'jeunesguineebundle_slides';
    }


}
