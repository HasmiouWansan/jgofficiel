<?php

namespace JeunesGuineeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RepresentantsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom',null,array('attr'=>array('required'=>true,'class'=>'input','placeholder'=>'Nom & Prenom')))
                ->add('fonction',null,array('attr'=>array('required'=>true,'class'=>'input','placeholder'=>'Fonction dans l\'ONG')))
                ->add('biographie','textarea',array('required'=>true,'attr'=>array('class'=>'ckeditor','placeholder'=>'Adresse')))
                ->add('coordinations','entity', array('class'=>'JeunesGuineeBundle\Entity\Coordinations',
                                                 'property'=>'Appelation',
                                                 'required'=>true, 
                                                 'label'=>true,
                                                 'attr'=>array('class'=>'form-control')))
                ->add('photo', new MediasType());
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JeunesGuineeBundle\Entity\Representants'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'jeunesguineebundle_representants';
    }


}
