<?php

namespace JeunesGuineeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CoordinationsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('appelation',null,array('attr'=>array('required'=>true,'class'=>'input','placeholder'=>'Appelation Ex Paris')))
                ->add('description','textarea',array('required'=>true,'attr'=>array('class'=>'ckeditor','placeholder'=>'Adresse')))
                ->add('telephone',null,array('attr'=>array('required'=>true,'class'=>'input','placeholder'=>'Numéro de contact')))
                ->add('email',null,array('attr'=>array('required'=>true,'class'=>'input','placeholder'=>'Email de la coordination')))
                ->add('adresse','textarea',array('required'=>false,'attr'=>array('class'=>'input')));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JeunesGuineeBundle\Entity\Coordinations'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'jeunesguineebundle_coordinations';
    }


}
