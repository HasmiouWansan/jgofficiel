<?php

namespace JeunesGuineeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JeunesGuineeBundle\Form\MediasType;

class PublicationsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titre',null,array('attr'=>array('class'=>'input',
                                                               'placeholder'=>'Titre du Contenu'))
                      )
                ->add('contenu','textarea',array('attr'=>array('class'=>'ckeditor',
                                                               'placeholder'=>'Contenu de la publication'))
                      )
                ->add('publier')
                ->add('image', new MediasType());
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JeunesGuineeBundle\Entity\Publications'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'jeunesguineebundle_publications';
    }


}
