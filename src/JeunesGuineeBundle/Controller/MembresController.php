<?php

namespace JeunesGuineeBundle\Controller;

use JeunesGuineeBundle\Entity\Membres;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Membre controller.
 *
 */
class MembresController extends Controller
{
    /**
     * Lists all membre entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $membres = $em->getRepository('JeunesGuineeBundle:Membres')->findAll();

        return $this->render('membres/index.html.twig', array(
            'membres' => $membres,
        ));
    }

    /**
     * Creates a new membre entity.
     *
     */
    public function newAction(Request $request)
    {
        $membre = new Membres();
        $form = $this->createForm('JeunesGuineeBundle\Form\MembresType', $membre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($membre);
            $em->flush();

            return $this->redirectToRoute('membres_show', array('id' => $membre->getId()));
        }

        return $this->render('membres/new.html.twig', array(
            'membre' => $membre,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a membre entity.
     *
     */
    public function showAction(Membres $membre)
    {
        $deleteForm = $this->createDeleteForm($membre);

        return $this->render('membres/show.html.twig', array(
            'membre' => $membre,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing membre entity.
     *
     */
    public function editAction(Request $request, Membres $membre)
    {
        $deleteForm = $this->createDeleteForm($membre);
        $editForm = $this->createForm('JeunesGuineeBundle\Form\MembresType', $membre);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('membres_edit', array('id' => $membre->getId()));
        }

        return $this->render('membres/edit.html.twig', array(
            'membre' => $membre,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a membre entity.
     *
     */
    public function deleteAction(Request $request, Membres $membre)
    {
        $form = $this->createDeleteForm($membre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($membre);
            $em->flush();
        }

        return $this->redirectToRoute('membres_index');
    }

    /**
     * Creates a form to delete a membre entity.
     *
     * @param Membres $membre The membre entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Membres $membre)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('membres_delete', array('id' => $membre->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
