<?php

namespace JeunesGuineeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use JeunesGuineeBundle\Entity\Membres;
use JeunesGuineeBundle\Entity\Abonnes;
use JeunesGuineeBundle\Entity\Messages;

use JeunesGuineeBundle\Form\AdhesionType;
use JeunesGuineeBundle\Form\ContactType;
use JeunesGuineeBundle\Form\AbonnesType;


class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        //findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
        $slide = $em->getRepository('JeunesGuineeBundle:Slides')->findOneByActif(1);
        $publications = $em->getRepository('JeunesGuineeBundle:Publications')->byLast();
        $presentation = $em->getRepository('JeunesGuineeBundle:Publications')->findOneByTitre('presentation');
        $evenements = $em->getRepository('JeunesGuineeBundle:Evenements')->byAvenir();
        
        return $this->render('JeunesGuineeBundle:Default:index.html.twig',array('slide'=>$slide,
                                                                                'publications'=>$publications,
                                                                                'presentation'=>$presentation,
                                                                                'evenements'=>$evenements));
    }

    public function navAction()
    {
        $em = $this->getDoctrine()->getManager();
        $nbMessage = $em->getRepository('JeunesGuineeBundle:Messages')->countByView();
        $nbAdherant = $em->getRepository('JeunesGuineeBundle:Membres')->countByValide();
        
        return $this->render('sidebar-left.html.twig',array('nbMessage'=>$nbMessage,
                                                            'nbAdherant'=>$nbAdherant));
    }

    public function footerAction(){
        $em = $this->getDoctrine()->getManager();
        $coordonees = $em->getRepository('JeunesGuineeBundle:Coordinations')->findOneByAppelation("Paris");
                
        return $this->render('footer.html.twig',array('coordonee'=>$coordonees));
    }


    /**
     * @Route("abonnement")
     */
    public function newletterAction(Request $request){
        $abonnes = new Abonnes();
        $form = $this->createForm(new AbonnesType(), $abonnes);
       
        if($this->get('request')->getMethod()=='POST'){
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($abonnes);
                $em->flush();
                $this->get('session')->getFlashBag()->add('notice', 'Votre abonnement aux Newsletters à bien été prise en compte. Desormais vous recevrez des alerte sur nos publications.');
                return $this->redirect($this->generateUrl('jeunesguinee_default_index'));
            }else{
                  $this->get('session')->getFlashBag()->add('error', 'Votre abonnement aux Newsletters n\'a pas abouti nous nous activons pour resoudre ce problème.');
                return $this->redirect($this->generateUrl('jeunesguinee_default_index'));
            }
        }
        return $this->render('JeunesGuineeBundle:Default:newletter.html.twig', array('form'=>$form->createView()));
    }

    public function desabonnerAction($id){
        $em = $this->getDoctrine()->getManager();
        $abonne = $em->getRepository('JeunesGuineeBundle:Abonnes')->find($id);
        $em->remove($abonne);
        $em->flush();
        $this->get('session')->getFlashBag()->add('notice', 'Votre désabonnement aux Newsletters à bien été prise en compte. Desormais vous ne recevrez  plus d\'alertes de nos publications.');
        return $this->redirect($this->generateUrl('jeunesguinee_default_index'));
    }

    /**
     * @Route("qsn")
     */
    public function qsnAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $presentation = $em->getRepository('JeunesGuineeBundle:Publications')->findOneByTitre('presentation');
        $objectif_desc = $em->getRepository('JeunesGuineeBundle:Publications')->findOneByTitre('objectifs_description');
        $objectif_list = $em->getRepository('JeunesGuineeBundle:Publications')->findOneByTitre('objectifs_list');
        $coordinations = $em->getRepository('JeunesGuineeBundle:Coordinations')->findAll();
        $paris = $em->getRepository('JeunesGuineeBundle:Representants')->findByCoordinations(1);
        
        return $this->render('JeunesGuineeBundle:Default:qsn.html.twig', array('presentation'=>$presentation,
                                                                               'objectif_desc'=>$objectif_desc,
                                                                               'objectif_list'=>$objectif_list,
                                                                               'paris'=>$paris,
                                                                               'coordinations'=>$coordinations));
    }

    /**
     * @Route("blog")
     */
    public function blogAction()
    {
        $em = $this->getDoctrine()->getManager();
        $publications = $em->getRepository('JeunesGuineeBundle:Publications')->findByPublier(true);
        return $this->render('JeunesGuineeBundle:Default:publications.html.twig',array('publications'=>$publications));
    }

    
     /**
     * @Route("publication")
     */
    public function showPublicationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $publication = $em->getRepository('JeunesGuineeBundle:Publications')->findOneById($id);
        return $this->render('JeunesGuineeBundle:Default:publication.html.twig',array('publication'=>$publication));
    }

    /**
     * @Route("evenements")
     */
    public function showEvenementsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $evenements = $em->getRepository('JeunesGuineeBundle:Evenements')->findByPublier(true);
        return $this->render('JeunesGuineeBundle:Default:evenements.html.twig',array('evenements'=>$evenements));
    }

    /**
     * @Route("contact")
     */
    public function contactAction(Request $request)
    {
        //On initialise une session
        $session = $this->getRequest()->getSession();
        $message = new Messages();
        $form = $this->createForm(new ContactType(), $message);
        
        if($this->get('request')->getMethod()=='POST'){
            $form->handleRequest($request);
            if ($form->isValid()) {

                $mailer = $this->get('mailer');
                $emailRec = $this->container->getParameter('mailer_user');

                $message = \Swift_Message::newInstance()
                        ->setSubject($form['sujet']->getData())
                        ->setFrom($form['emailExp']->getData())
                        ->setTo('amiroudia@gmail.com')
                        ->setCharset('utf-8')
                        ->setContentType('text/plain')
                        ->setBody($form['message']->getData());

                if (!$mailer->send($message, $failures)) {
                    $this->get('session')->getFlashBag()->add('error', 'Une erreur s\'est produite lors de la transmission de votre message, nous fesons de notre mieux pour trouvez la solution.');    
                }else{

                    $em = $this->getDoctrine()->getManager();
                    $messageDB = new Messages();
                    $messageDB->setSujet($form['sujet']->getData());
                    $messageDB->setEmailExp($form['emailExp']->getData());
                    $messageDB->setEmailRec($emailRec);
                    $messageDB->setMessage($form['message']->getData());
                    
                    $em->persist($messageDB);
                    $em->flush();

                    $mailer->send($message);
                    $this->get('session')->getFlashBag()->add('notice', 'Votre message à été transmi avec succès, nous reviendons vers vous le plus vite que possible.');
                }

                return $this->redirect($this->generateUrl('contact'));


            }
        }
        return $this->render('JeunesGuineeBundle:Default:contact.html.twig', array('form'=>$form->createView()));
    }

    /**
     * @Route("adhesion")
     */
    public function adhesionAction(Request $request)
    {
        $session = $this->getRequest()->getSession();
        $membre = new Membres();
        $abonne = new Abonnes();
        $form = $this->createForm(new AdhesionType(), $membre);
        $form->handleRequest($request);
        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $abonne->setEmail($form['email']->getData());
            $em->persist($membre);
            $em->persist($abonne);

            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'Votre demande d\'adhesion à été transmise avec succès. Vous recevrez un message vous invitant à participer à notre prochaine assemblée générale.');
           return $this->redirect($this->generateUrl('adhesion'));
        }

        return $this->render('JeunesGuineeBundle:Default:adhesion.html.twig', array('form'=>$form->createView()));
    }
}
