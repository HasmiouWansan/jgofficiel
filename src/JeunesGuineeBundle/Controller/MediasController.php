<?php

namespace JeunesGuineeBundle\Controller;

use JeunesGuineeBundle\Entity\Medias;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Media controller.
 *
 */
class MediasController extends Controller
{
    /**
     * Lists all media entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $medias = $em->getRepository('JeunesGuineeBundle:Medias')->findAll();

        return $this->render('medias/index.html.twig', array(
            'medias' => $medias,
        ));
    }

    /**
     * Creates a new media entity.
     *
     */
    public function newAction(Request $request)
    {
        $media = new Medias();
        $form = $this->createForm('JeunesGuineeBundle\Form\MediasType', $media);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($media);
            $em->flush();

            return $this->redirectToRoute('medias_show', array('id' => $media->getId()));
        }

        return $this->render('medias/new.html.twig', array(
            'media' => $media,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a media entity.
     *
     */
    public function showAction(Medias $media)
    {
        $deleteForm = $this->createDeleteForm($media);

        return $this->render('medias/show.html.twig', array(
            'media' => $media,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing media entity.
     *
     */
    public function editAction(Request $request, Medias $media)
    {
        $deleteForm = $this->createDeleteForm($media);
        $editForm = $this->createForm('JeunesGuineeBundle\Form\MediasType', $media);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('medias_edit', array('id' => $media->getId()));
        }

        return $this->render('medias/edit.html.twig', array(
            'media' => $media,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a media entity.
     *
     */
    public function deleteAction(Request $request, Medias $media)
    {
        $form = $this->createDeleteForm($media);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($media);
            $em->flush();
        }

        return $this->redirectToRoute('medias_index');
    }

    /**
     * Creates a form to delete a media entity.
     *
     * @param Medias $media The media entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Medias $media)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('medias_delete', array('id' => $media->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
