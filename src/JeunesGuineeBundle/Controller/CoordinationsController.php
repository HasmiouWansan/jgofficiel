<?php

namespace JeunesGuineeBundle\Controller;

use JeunesGuineeBundle\Entity\Coordinations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Coordination controller.
 *
 */
class CoordinationsController extends Controller
{
    /**
     * Lists all coordination entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $coordinations = $em->getRepository('JeunesGuineeBundle:Coordinations')->findAll();

        return $this->render('coordinations/index.html.twig', array(
            'coordinations' => $coordinations,
        ));
    }

    /**
     * Creates a new coordination entity.
     *
     */
    public function newAction(Request $request)
    {
        $coordination = new Coordinations();
        $form = $this->createForm('JeunesGuineeBundle\Form\CoordinationsType', $coordination);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($coordination);
            $em->flush();

            return $this->redirectToRoute('admin_Coordination_show', array('id' => $coordination->getId()));
        }

        return $this->render('coordinations/new.html.twig', array(
            'coordination' => $coordination,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a coordination entity.
     *
     */
    public function showAction(Coordinations $coordination)
    {
        $deleteForm = $this->createDeleteForm($coordination);

        return $this->render('coordinations/show.html.twig', array(
            'coordination' => $coordination,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing coordination entity.
     *
     */
    public function editAction(Request $request, Coordinations $coordination)
    {
        $deleteForm = $this->createDeleteForm($coordination);
        $editForm = $this->createForm('JeunesGuineeBundle\Form\CoordinationsType', $coordination);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_Coordination_edit', array('id' => $coordination->getId()));
        }

        return $this->render('coordinations/edit.html.twig', array(
            'coordination' => $coordination,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a coordination entity.
     *
     */
    public function deleteAction(Request $request, Coordinations $coordination)
    {
        $form = $this->createDeleteForm($coordination);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($coordination);
            $em->flush();
        }

        return $this->redirectToRoute('admin_Coordination_index');
    }

    /**
     * Creates a form to delete a coordination entity.
     *
     * @param Coordinations $coordination The coordination entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Coordinations $coordination)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_Coordination_delete', array('id' => $coordination->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
