<?php

namespace JeunesGuineeBundle\Controller;

use JeunesGuineeBundle\Entity\Abonnes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Abonne controller.
 *
 */
class AbonnesController extends Controller
{
    /**
     * Lists all abonne entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $abonnes = $em->getRepository('JeunesGuineeBundle:Abonnes')->findAll();

        return $this->render('abonnes/index.html.twig', array(
            'abonnes' => $abonnes,
        ));
    }

    /**
     * Creates a new abonne entity.
     *
     */
    public function newAction(Request $request)
    {
        $abonne = new Abonnes();
        $form = $this->createForm('JeunesGuineeBundle\Form\AbonnesType', $abonne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($abonne);
            $em->flush();

            return $this->redirectToRoute('abonnes_show', array('id' => $abonne->getId()));
        }

        return $this->render('abonnes/new.html.twig', array(
            'abonne' => $abonne,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a abonne entity.
     *
     */
    public function showAction(Abonnes $abonne)
    {
        $deleteForm = $this->createDeleteForm($abonne);

        return $this->render('abonnes/show.html.twig', array(
            'abonne' => $abonne,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing abonne entity.
     *
     */
    public function editAction(Request $request, Abonnes $abonne)
    {
        $deleteForm = $this->createDeleteForm($abonne);
        $editForm = $this->createForm('JeunesGuineeBundle\Form\AbonnesType', $abonne);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('abonnes_edit', array('id' => $abonne->getId()));
        }

        return $this->render('abonnes/edit.html.twig', array(
            'abonne' => $abonne,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a abonne entity.
     *
     */
    public function deleteAction(Request $request, Abonnes $abonne)
    {
        $form = $this->createDeleteForm($abonne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($abonne);
            $em->flush();
        }

        return $this->redirectToRoute('abonnes_index');
    }

    /**
     * Creates a form to delete a abonne entity.
     *
     * @param Abonnes $abonne The abonne entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Abonnes $abonne)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('abonnes_delete', array('id' => $abonne->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
