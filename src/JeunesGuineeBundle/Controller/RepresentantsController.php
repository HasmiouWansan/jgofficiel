<?php

namespace JeunesGuineeBundle\Controller;

use JeunesGuineeBundle\Entity\Representants;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use JeunesGuineeBundle\Entity\Coordinations;

/**
 * Representant controller.
 *
 */
class RepresentantsController extends Controller
{
    /**
     * Lists all representant entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $representants = $em->getRepository('JeunesGuineeBundle:Representants')->findAll();

        return $this->render('representants/index.html.twig', array(
            'representants' => $representants,
        ));
    }

    /**
     * Lists representant entities by Coordinations.
     *
     */
    public function byCordinationAction(Coordinations $id)
    {
        $em = $this->getDoctrine()->getManager();

        $representants = $em->getRepository('JeunesGuineeBundle:Representants')->byCordination($id);

        return $this->render('representants/index.html.twig', array(
            'representants' => $representants,
        ));
    }

    /**
     * Creates a new representant entity.
     *
     */
    public function newAction(Request $request)
    {
        $representant = new Representants();
        $form = $this->createForm('JeunesGuineeBundle\Form\RepresentantsType', $representant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($representant);
            $em->flush();

            return $this->redirectToRoute('admin_representants_show', array('id' => $representant->getId()));
        }

        return $this->render('representants/new.html.twig', array(
            'representant' => $representant,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a representant entity.
     *
     */
    public function showAction(Representants $representant)
    {
        $deleteForm = $this->createDeleteForm($representant);

        return $this->render('representants/show.html.twig', array(
            'representant' => $representant,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing representant entity.
     *
     */
    public function editAction(Request $request, Representants $representant)
    {
        $deleteForm = $this->createDeleteForm($representant);
        $editForm = $this->createForm('JeunesGuineeBundle\Form\RepresentantsType', $representant);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_representants_edit', array('id' => $representant->getId()));
        }

        return $this->render('representants/edit.html.twig', array(
            'representant' => $representant,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a representant entity.
     *
     */
    public function deleteAction(Request $request, Representants $representant)
    {
        $form = $this->createDeleteForm($representant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($representant);
            $em->flush();
        }

        return $this->redirectToRoute('admin_representants_index');
    }

    /**
     * Creates a form to delete a representant entity.
     *
     * @param Representants $representant The representant entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Representants $representant)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_representants_delete', array('id' => $representant->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
