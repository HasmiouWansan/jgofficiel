<?php

namespace JeunesGuineeBundle\Controller;

use JeunesGuineeBundle\Entity\Slides;
use JeunesGuineeBundle\Entity\Medias;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Slide controller.
 *
 */
class SlidesController extends Controller
{
    /**
     * Lists all slide entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $slides = $em->getRepository('JeunesGuineeBundle:Slides')->findAll();

        return $this->render('slides/index.html.twig', array(
            'slides' => $slides,
        ));
    }

    public function addImage(Medias $fichier){
        $media = $fichier;

        $em = $this->getDoctrine()->getManager();
        $em->persist($media);
        $em->flush();

        return $media->getAssetPath();
    }

    /**
     * Creates a new slide entity.
     *
     */
    public function newAction(Request $request)
    {
        $slide = new Slides();

        $form = $this->createForm('JeunesGuineeBundle\Form\SlidesType');
        //$form->handleRequest($request);
        

        $gallerie = array();
        if($this->get('request')->getMethod()=='POST'){
            $form->bind($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $slide->setTitre($form['titre']->getData());
                
                $gallerie[0]=$this->addImage($form['photo1']->getData());
                $gallerie[1]=$this->addImage($form['photo2']->getData());
                $gallerie[2]=$this->addImage($form['photo3']->getData());

                $slide->setPhotos($gallerie);

                $em->persist($slide);
                $em->flush();

                return $this->redirectToRoute('slides_show', array('id' => $slide->getId()));
            }
    }
        return $this->render('slides/new.html.twig', array(
            'slide' => $slide,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a slide entity.
     *
     */
    public function showAction(Slides $slide)
    {
        $deleteForm = $this->createDeleteForm($slide);

        return $this->render('slides/show.html.twig', array(
            'slide' => $slide,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing slide entity.
     *
     */
    public function editAction(Request $request, Slides $slide)
    {
        $deleteForm = $this->createDeleteForm($slide);
        $editForm = $this->createForm('JeunesGuineeBundle\Form\SlidesType');
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('slides_edit', array('id' => $slide->getId()));
        }

        return $this->render('slides/edit.html.twig', array(
            'slide' => $slide,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a slide entity.
     *
     */
    public function deleteAction(Request $request, Slides $slide)
    {
        $form = $this->createDeleteForm($slide);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($slide);
            $em->flush();
        }

        return $this->redirectToRoute('slides_index');
    }

    /**
     * Creates a form to delete a slide entity.
     *
     * @param Slides $slide The slide entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Slides $slide)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('slides_delete', array('id' => $slide->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
